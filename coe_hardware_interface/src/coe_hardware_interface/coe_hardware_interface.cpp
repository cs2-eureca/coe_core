
#include <coe_hardware_interface/coe_hardware_interface.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>


namespace itia_hardware_interface
{


CoeRobotHW::CoeRobotHW(const std::map<std::string,std::pair<std::string,int>>& devices)
{
  m_devices=devices;
//   for (const std::pair<std::string,std::pair<std::string,int>>& device: devices)
//     ROS_FATAL("coe_ros_plugins: %s,\t%d",device.first.c_str(),device.second);
}

bool CoeRobotHW::init(ros::NodeHandle& root_nh, ros::NodeHandle& robot_hw_nh)
{
  ROS_DEBUG("[%sSTART%s%s] Initialize the CoeRobotHw",BOLDMAGENTA,RESET,GREEN);
  if (!itia_hardware_interface::BasicRobotHW::init(root_nh, robot_hw_nh))
  {
    ROS_ERROR("[%s] BasicRobotHW error",robot_hw_nh.getNamespace().c_str());
    return false;
  }

  std::string soem_ns;
  if (!robot_hw_nh.getParam("soem_ns",soem_ns))
  {
    ROS_ERROR("Failed in extracting the param '%s/soem_ns' is not set",robot_hw_nh.getNamespace().c_str());
    return false;
  }
  boost::shared_ptr<coe_driver::CoeHwPlugin> device_instance;
  boost::shared_ptr<pluginlib::ClassLoader<coe_driver::CoeHwPlugin>> planner_plugin_loader;

  m_pos .resize(m_devices.size());
  m_vel .resize(m_devices.size());
  m_eff .resize(m_devices.size());
  m_tpos.resize(m_devices.size());
  m_tvel.resize(m_devices.size());
  m_teff.resize(m_devices.size());

  unsigned int iDev=0;
  for (const std::pair<std::string,std::pair<std::string,int>>& device: m_devices)
  {

    int address=(device.second).second;
    std::string device_type=(device.second).first;
    std::string name=device.first;

    ROS_FATAL("device_name=%s, adress address=%d",device.first.c_str(),address);

    try
    {
      planner_plugin_loader.reset(new pluginlib::ClassLoader<coe_driver::CoeHwPlugin>("coe_driver", "coe_driver::CoeHwPlugin"));
    }
    catch (pluginlib::PluginlibException& ex)
    {
      ROS_FATAL_STREAM("Exception while creating device plugin loader " << ex.what());
      continue;
    }


    try
    {
      ROS_WARN("Loading instance '%s' of type: '%s'",name.c_str(),device_type.c_str());
      planner_plugin_loader->loadLibraryForClass(device_type);
      ROS_WARN("Is Class Available? %s", ( planner_plugin_loader->isClassAvailable(device_type) ? "YES" : "NO" ) );
      ROS_WARN("Name of the class? %s", planner_plugin_loader->getName(device_type).c_str() );

      ROS_WARN("Create Instance '%s' of type: '%s'",name.c_str(),device_type.c_str());

      device_instance = planner_plugin_loader->createInstance( device_type );

      if (!device_instance->initialize(root_nh,soem_ns,address))
      {
        ROS_ERROR_STREAM("Could not initialize device instance: " << name);
        continue;
      }
    }
    catch (pluginlib::PluginlibException& ex)
    {
      const std::vector<std::string>& classes = planner_plugin_loader->getDeclaredClasses();
      std::stringstream ss;
      for (std::size_t i = 0; i < classes.size(); ++i)
        ss << classes[i] << " ";
      ROS_ERROR_STREAM("Exception while loading planner '" << name << "': " << ex.what() << std::endl     << "Available plugins: " << ss.str());
      continue;
    }

    if (device_instance->isActuator())
    {
      ROS_FATAL("Device %s (type: %s) is an actuator DS402",name.c_str(),device_type.c_str());
      device_instance->jointStateHandle( &m_pos.at(iDev), &m_vel.at(iDev), &m_eff.at(iDev));

      hardware_interface::JointStateHandle js_handle(name, m_pos.at(iDev), m_vel.at(iDev), m_eff.at(iDev));
      m_js_jh.registerHandle(js_handle);

      device_instance->jointCommandHandle( &m_tpos.at(iDev), &m_tvel.at(iDev), &m_teff.at(iDev));

      std::vector<std::string> available_modes= device_instance->getStateNames();

      bool can_use_p, can_use_v, can_use_e, can_use_pve, can_use_ve;
      can_use_p=can_use_v=can_use_e=can_use_pve=can_use_ve=false;

      for (const std::string& mode: available_modes)
      {
        coe_core::ds402::ModeOperationID id=coe_core::ds402::to_modeofoperationid(mode);
        switch (id)
        {

//           case coe_core::ds402::MOO_PROFILED_POSITION_MODE:
//           case coe_core::ds402::MOO_INTERPOLATED_POSITION_MODE:
//           case coe_core::ds402::MOO_HOMING_MODE:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE:
            can_use_p=true;
            break;
//           case coe_core::ds402::MOO_PROFILED_VELOCITY_MODE:
//           case coe_core::ds402::MOO_VELOCITY_MODE:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE:
            can_use_v=true;
            break;
//           case coe_core::ds402::MOO_PROFILED_TORQUE_MODE:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE:
            can_use_e=true;
            break;
          default:
            break;
        }
      }
      if (can_use_p)
      {
        hardware_interface::JointHandle pos_handle(js_handle,m_tpos.at(iDev));
        m_p_jh.registerHandle( pos_handle );
      }
      if (can_use_v)
      {
        hardware_interface::JointHandle vel_handle(js_handle,m_tvel.at(iDev));
        m_v_jh.registerHandle( vel_handle );
      }
      if (can_use_e)
      {
        hardware_interface::JointHandle eff_handle(js_handle,m_teff.at(iDev));
        m_e_jh.registerHandle( eff_handle );
      }
      if (can_use_pve)
      {
        hardware_interface::PosVelEffJointHandle pve_handle(js_handle,m_tpos.at(iDev), m_tvel.at(iDev), m_teff.at(iDev));
        m_pve_jh.registerHandle(pve_handle);
      }
      if (can_use_ve)
      {
        hardware_interface::VelEffJointHandle ve_handle(js_handle,m_tvel.at(iDev), m_teff.at(iDev));
        m_ve_jh.registerHandle(ve_handle);
      }
      m_joint_to_device_map.insert(std::make_pair(name,device_instance));
      ROS_FATAL("ok");
    }


    if (device_instance->hasAnalogInputs())
    {
      std::vector<std::string> analog_inputs=device_instance->getAnalogInputNames();
      m_ai.resize(analog_inputs.size());
      for (std::size_t idx=0;idx<analog_inputs.size();idx++)
      {
        m_ai.at(idx) = device_instance->analogInputValueHandle(analog_inputs.at(idx));
        hardware_interface::AnalogStateHandle handle(analog_inputs.at(idx),m_ai.at(idx));
        m_as_jh.registerHandle(handle);
      }
    }

    if (device_instance->hasAnalogOutputs())
    {
      std::vector<std::string> analog_outputs=device_instance->getAnalogOutputNames();
      m_ao.resize(analog_outputs.size());
      for (std::size_t idx=0;idx<analog_outputs.size();idx++)
      {
        m_ao.at(idx) = device_instance->analogOutputValueHandle(analog_outputs.at(idx));
        hardware_interface::AnalogStateHandle handle(analog_outputs.at(idx),m_ao.at(idx));
        hardware_interface::AnalogHandle command_handle(handle,m_ao.at(idx));
        m_as_jh.registerHandle(handle);
        m_ao_jh.registerHandle(command_handle);
      }
    }

    if (device_instance->hasDigitalInputs())
    {
      std::vector<std::string> digital_inputs=device_instance->getDigitalInputNames(); 
      m_di.resize(digital_inputs.size());
      for (std::size_t idx=0;idx<digital_inputs.size();idx++)
      {
        m_di.at(idx) = device_instance->digitalInputValueHandle(digital_inputs.at(idx));
        hardware_interface::DigitalStateHandle handle(digital_inputs.at(idx),m_di.at(idx));
        m_ds_jh.registerHandle(handle);
      }
    }

    if (device_instance->hasDigitalOutputs())
    {
      std::vector<std::string> digital_outputs=device_instance->getDigitalOutputNames();
      m_do.resize(digital_outputs.size());
      for (std::size_t idx=0;idx<digital_outputs.size();idx++)
      {
        m_do.at(idx) = device_instance->digitalOutputValueHandle(digital_outputs.at(idx));
        hardware_interface::DigitalStateHandle handle(digital_outputs.at(idx),m_do.at(idx));
        hardware_interface::DigitalHandle command_handle(handle,m_do.at(idx));
        m_ds_jh.registerHandle(handle);
        m_do_jh.registerHandle(command_handle);
      }
    }

    m_devices_types_map.insert    (std::make_pair( name, device_type ) );
    m_devices_map.insert          (std::make_pair( name, device_instance ) );
    m_planner_plugin_loader.insert(std::make_pair( name, planner_plugin_loader ) );

    ROS_FATAL("Device %s (type: %s) is an actuator DS402 %sOK%s",name.c_str(),device_type.c_str(), BOLDGREEN, RESET);
    iDev++;
  }

  registerInterface(&m_js_jh);
  registerInterface(&m_p_jh);
  registerInterface(&m_v_jh);
  registerInterface(&m_e_jh);
  registerInterface(&m_ve_jh);
  registerInterface(&m_pve_jh);
  registerInterface(&m_as_jh);
  registerInterface(&m_ao_jh);
  registerInterface(&m_ds_jh);
  registerInterface(&m_do_jh);
  ROS_DEBUG("[%s  OK %s%s] Initialize the CoeRobotHw",BOLDWHITE,RESET,GREEN);
  return true;
}

bool CoeRobotHW::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  ROS_DEBUG("[%sSTART%s%s] Prepare Switch",BOLDMAGENTA,RESET,GREEN);
  for (const hardware_interface::ControllerInfo& controller: start_list)
  {
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      ROS_FATAL("controller name: %s type: %s hi: %s",controller.name.c_str(),controller.type.c_str(),res.hardware_interface.c_str());

      coe_core::ds402::ModeOperationID new_id;
      std::string new_id_string;
      if ( !controller.type.compare("itia/control/HomingController") )
      {
        new_id=coe_core::ds402::MOO_HOMING_MODE;
        new_id_string="MOO_HOMING";
        ROS_FATAL("QUI");
      }
      else if (res.hardware_interface=="hardware_interface::PositionJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION_MODE;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_POSITION";
      }
      else if (res.hardware_interface=="hardware_interface::VelocityJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY_MODE;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_VELOCITY";
      }
      else if (res.hardware_interface=="hardware_interface::EffortJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE_MODE;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_TORQUE";
      }
      else
      {
        ROS_FATAL("Nothing to do");
        continue;
      }
      ROS_FATAL("new mode: %s",new_id_string.c_str());

      ROS_FATAL("resources size: %zu",res.resources.size());
      for( const std::string& joint: res.resources)
        m_joint_to_device_map.at(joint)->setSoftRT();
      
      for (const std::string& joint: res.resources)
      {
        ROS_FATAL("  joint %s",joint.c_str());

        // check if request mode is available
        std::vector<std::string> available_modes= m_joint_to_device_map.at(joint)->getStateNames();
        bool is_present=false;
        for (const std::string& mode: available_modes)
        {
          coe_core::ds402::ModeOperationID id=coe_core::ds402::to_modeofoperationid(mode);
          if (id==new_id)
          {
            is_present=true;
            break;
          }
        }
        if (!is_present)
        {
          ROS_ERROR("Controller %s can not be run of joint %s",controller.name.c_str(),joint.c_str());
          return false;
        }

        coe_core::ds402::ModeOperationID act_id=coe_core::ds402::to_modeofoperationid(m_joint_to_device_map.at(joint)->getActualState());
        if (act_id==new_id)
        {
          ROS_WARN("Controller %s is already in mode %s",controller.name.c_str(),new_id_string.c_str());
//           continue;
        }

        try
        {
          if( !m_joint_to_device_map.at(joint)->setTargetState(new_id_string) )
          {
            ROS_ERROR("IMPOSSIBLE TO SWITCH JOINT %s TO  STATE: %s.",joint.c_str(),new_id_string.c_str());
            return false;
          }
          ROS_FATAL("Controller %s is switched in mode %s",controller.name.c_str(),new_id_string.c_str());
        }
        catch ( std::exception& e )
        {
          ROS_ERROR("IMPOSSIBLE TO SWITCH JOINT %s TO  STATE: %s.\nerror: %s",joint.c_str(),new_id_string.c_str(),e.what());
          return false;
        }
      }
    
      for( const std::string& joint: res.resources)
        m_joint_to_device_map.at(joint)->setHardRT();
    }
  }
  ROS_DEBUG("[%s  OK %s%s] Prepare Switch",BOLDWHITE,RESET,GREEN);
  return true;
}


void CoeRobotHW::read(const ros::Time& time, const ros::Duration& period)
{
  for (auto& p: m_devices_map)
  { 
    if (!p.second->read())
    {
      ROS_WARN("[ %s ] Error Read ", p.second->getUniqueId ().c_str() );
      m_status = with_error;
    }
  }
}

void CoeRobotHW::write(const ros::Time& time, const ros::Duration& period)
{
  for (auto& p: m_devices_map)
  {
    if (!p.second->write())
    {
      ROS_WARN("EHI! You have to add the error management");
    }
  }
}




void CoeRobotHW::shutdown()
{
  ROS_INFO("DESTROY");
  for( auto & device_info : m_devices_types_map )
  {
    std::string name = device_info.first;
    std::string type = device_info.second;
    m_devices_map.at(name)->setSoftRT();
    ros::Duration(0.001).sleep();
    ROS_INFO("Delete Instance '%s' from heap ", name.c_str() );
    m_devices_map.at(name).reset();
    ROS_INFO("Unconnect '%s' from heap ", name.c_str() );
    m_joint_to_device_map.at(name).reset();
    ROS_INFO("Unload plugin '%s' from heap ", type.c_str() );
    m_planner_plugin_loader.at(name)->unloadLibraryForClass( type );
  }
  m_joint_to_device_map.clear();
  m_devices_map.clear();
  m_planner_plugin_loader.clear();
  
  
}


}


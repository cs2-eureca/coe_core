#include <coe_hardware_interface/coe_hi_nodelet.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::CoeHwIfaceNodelet, nodelet::Nodelet) 



namespace itia
{
  namespace control
  {
    void CoeHwIfaceNodelet::onInit()
    {
      /*
      ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
      */  
      m_console_name = getPrivateNodeHandle().getNamespace()+" type: CoeHwIfaceNodelet";
      ROS_INFO("[%s] STARTING", m_console_name.c_str());
      
      m_stop = false;
      
      std::map<std::string,std::pair<std::string,int>> devices;
      XmlRpc::XmlRpcValue config;
      if (!getPrivateNodeHandle().getParam("coe_ros_plugins", config))
      {
        ROS_ERROR_STREAM(getPrivateNodeHandle().getNamespace()+"/coe_ros_plugins' does not exist");
        ROS_ERROR("ERROR DURING STARTING HARDWARE INTERFACE '%s'", getPrivateNodeHandle().getNamespace().c_str());
        return;
      }
      if (config.getType() != XmlRpc::XmlRpcValue::TypeArray)
      {
        ROS_ERROR_STREAM(getPrivateNodeHandle().getNamespace()+"/coe_ros_plugins' is not a list of devices type and address");
        return;
      }
      ROS_DEBUG_STREAM("Scan of feasible coe_ros_plugins list (number of plugin:" << config.size() << ")"  );
      for(auto i=0; i < config.size(); i++) 
      {
        XmlRpc::XmlRpcValue cfg = config[i];  
        if( cfg.getType() != XmlRpc::XmlRpcValue::TypeStruct)
        {
          ROS_WARN("The element #%u is not a struct", i);
          continue;
        }
        if( !cfg.hasMember("name") )
        {
          ROS_WARN("The element #%u has not the field 'name'", i);
          continue;
        }
        if( !cfg.hasMember("type") )
        {
          ROS_WARN("The element #%u has not the field 'type'", i);
          continue;
        }
        if( !cfg.hasMember( "address" ) )
        {
          ROS_WARN("The element #%u has not the field 'address'", i);
          continue;
        }
      
        ROS_FATAL("Add device %s, type %s, address %d",((std::string)cfg["name"]).c_str(),((std::string)cfg["type"]).c_str(),(int)cfg["address"]);
        std::pair<std::string,std::pair<std::string,int>> new_device((std::string)cfg["name"],std::pair<std::string,int>((std::string)cfg["type"],(int)cfg["address"]));
        devices.insert(new_device);

      }
      
      m_hw.reset(new itia_hardware_interface::CoeRobotHW(devices));
      
      m_main_thread = std::thread(&itia::control::CoeHwIfaceNodelet::mainThread, this);
      
    };
    
    
  }
}
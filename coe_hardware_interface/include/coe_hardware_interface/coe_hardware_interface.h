#ifndef __ITIA_COE_HARDWARE_INTERFACE__
#define __ITIA_COE_HARDWARE_INTERFACE__

#include <diagnostic_updater/diagnostic_updater.h>

#include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>
#include <itia_basic_hardware_interface/analog_comand_interface.h>
#include <itia_basic_hardware_interface/digital_comand_interface.h>

#include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <itia_rutils/itia_rutils.h>
#include <coe_hardware_interface/device_xml_parser.h>
#include <pluginlib/class_loader.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>

namespace itia_hardware_interface
{
  
  class CoeRobotHW: public itia_hardware_interface::BasicRobotHW
  {
  public:
    CoeRobotHW(const std::map<std::string,std::pair<std::string,int>>& devices);
    virtual ~CoeRobotHW(){ROS_INFO("all is finished!");shutdown();};
    virtual void shutdown();
    virtual void read(const ros::Time& time, const ros::Duration& period);
    virtual void write(const ros::Time& time, const ros::Duration& period);
    
    virtual bool prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list);
    
    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle &robot_hw_nh) ;
    
    

    
  protected:
    
    hardware_interface::AnalogStateInterface    m_as_jh; //interface for reading Input/Output analog
    hardware_interface::AnalogCommandInterface  m_ao_jh; //interface for writing Output analog
    
    hardware_interface::DigitalStateInterface   m_ds_jh; //interface for reading Input/Output digital
    hardware_interface::DigitalCommandInterface m_do_jh; //interface for writing Output digital
    
    hardware_interface::JointStateInterface     m_js_jh; //interface for reading joint state
    hardware_interface::PositionJointInterface  m_p_jh; //interface for writing position target
    hardware_interface::VelocityJointInterface  m_v_jh; //interface for writing velocity target
    hardware_interface::EffortJointInterface    m_e_jh; //interface for writing effort target
    hardware_interface::PosVelEffJointInterface m_pve_jh;
    hardware_interface::VelEffJointInterface    m_ve_jh;
    
    std::vector<double*> m_pos ;
    std::vector<double*> m_vel ;
    std::vector<double*> m_eff ;
    std::vector<double*> m_tpos;
    std::vector<double*> m_tvel;
    std::vector<double*> m_teff;
    std::vector<double*> m_ai;
    std::vector<double*> m_ao;
    std::vector<bool*>   m_di;
    std::vector<bool*>   m_do;
    
    
    unsigned int m_nDevices;
    std::map<std::string,std::string > m_device_states;
    std::map<std::string,std::pair<std::string,int>> m_devices;
    std::map<std::string,boost::shared_ptr<pluginlib::ClassLoader<coe_driver::CoeHwPlugin>> > m_planner_plugin_loader; // NOTE IMPORTANT!!! it has to be declared before any boost::shared_ptr<coe_driver::CoeHwPlugin> (in this way, it will be destroyed later). 
    std::map<std::string,std::string>                                m_devices_types_map;
    std::map<std::string,boost::shared_ptr<coe_driver::CoeHwPlugin>> m_devices_map;
    std::map<std::string,boost::shared_ptr<coe_driver::CoeHwPlugin>> m_joint_to_device_map;
    
  };
}

#endif


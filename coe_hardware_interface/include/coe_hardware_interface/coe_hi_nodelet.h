#ifndef __COE_NODELET_HARDWARE_INTERFACE__ON_NODELET__
#define __COE_NODELET_HARDWARE_INTERFACE__ON_NODELET__

# include <controller_manager/controller_manager.h>
# include <nodelet/nodelet.h>
# include <thread>
# include <coe_hardware_interface/coe_hardware_interface.h>
# include <itia_basic_hardware_interface/basic_hi_nodelet.h>
namespace itia
{
  namespace control
  {
    
    class CoeHwIfaceNodelet : public BasicHwIfaceNodelet
    {
    public:
      virtual void onInit();
      
    protected:
    };
    
    
    
  }
}
# endif